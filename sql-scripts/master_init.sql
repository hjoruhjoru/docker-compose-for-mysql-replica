CREATE DATABASE IF NOT EXISTS calculator_db;
USE calculator_db;

CREATE TABLE IF NOT EXISTS calculator_commands (
    id INT AUTO_INCREMENT PRIMARY KEY,
    command_text TEXT NOT NULL,
    result DOUBLE NOT NULL,
    execution_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

GRANT REPLICATION SLAVE ON *.* TO 'admin'@'%';